# For Ubuntu 20.04 Rancher 2.5.9 and k3s v1.20.6+k3s1

# Ubuntu 20.04
# Install and set up static host for hostname and ip / mac
# Options:
# Install Ubuntu Server
# Language & keyboard layout as required
# Proxy - leave blank
# Mirror address: http://gb.archive.ubuntu.com/ubuntu or as required
# Disk: as required
# Your name:
# Server's name: r_host_server
# Username: up_to_you
# Password: Choose_a_good_one
# Install OpenSSH server: CHECK THE BOX!!
# Import SSH Identity: No
# Don't check any of the popular snaps
# When Install complete! shows up at top of screen remove CDs and select Reboot Now
# Press cr to escape from locked reboot screen "Can't ... CDROM ..."

# From WSL shell send ssh key to new server
ssh-copy-id -i /mnt/c/Users/Mike/.ssh/mitchell_ubuntu_id_ed25519.pub mike@mitchell
# Enter password: Choose_a_good_one when asked

# Can now ssh to server :
ssh up_to_you@r_host_server
ssh -i /home/mike/.ssh/mitchell_ubuntu_id_ed25519 mike@mitchell

# Login, change to superuser and install net tools 
# Change shell to superuser first:
sudo -s
cd ~

# All of the following now done from the ssh on the host WSL command line or other ssh terminal like putty etc

# OPTIONAL - Install net tools and any other preferred utilities
# apt install net-tools 


# To specify the K3s version, use the INSTALL_K3S_VERSION environment variable when running the K3s installation script.

# Get the install.sh for k3s at https://get.k3s.io/
# Or pipe it directly from curl: curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=vX.Y.Z sh -s -

# Following saves having to set up the INSTALL_K3S_VERSION environment variable and chooses version v1.20.6+k3s1
# curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.20.6+k3s1 sh -s -
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.21.4+k3s1 sh -s -

# May need to set the K3S_KUBECONFIG_MODE environment variable:
# curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" INSTALL_K3S_VERSION=v1.20.6+k3s1 sh -s -

# Install as follows to disable traefik and servicelb
#curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.20.6+k3s1 sh -s - --no-deploy traefik --disable servicelb

# Install as follows to disable traefik
# curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.20.6+k3s1 sh -s - --no-deploy traefik

# Install as follows to disable servicelb
# curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.20.6+k3s1 sh -s - --disable servicelb

# Or can perform / verify by editing/viewing /etc/systemd/system/k3s.service and editing the ExecStart line:
ExecStart=/usr/local/bin/k3s server --disable traefik --disable servicelb
# Then restart the service
systemctl restart k3s.service

# kubeconfig file may be read from /etc/rancher/k3s/k3s.yaml
cat /etc/rancher/k3s/k3s.yaml

# Copy the output from the cat .../k3s.yaml command and paste it into a file /home/mike/.kube/config on the WSL host
# Then edit the line:
#    server: https://127.0.0.1:6443
# to read :
#    server: https://r_host_server:6443
# and save 

# If you ever want to uninstall k3s run:
# /usr/local/bin/k3s-uninstall.sh


# Helm v3.2.x or higher is required to install or upgrade Rancher v2.5.
# For helm chart installs, docker needs to be installed, eg for docker v 20.10 run: 
curl https://releases.rancher.com/install-docker/20.10.sh | sh

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh --version v3.6.3


# Install cert-manager:
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.5.3/cert-manager.crds.yaml
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.5.3
# Or, to get around an issue installing Rancher later (Error: conversion webhook for cert-manager.io/v1beta1, Kind=Issuer failed: Post "https://cert-manager-webhook.cert-manager.svc:443/convert?timeout=30s": service "cert-manager-webhook" not found"), try:
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.5.3 --set installCRDs=true



# Install metallb:
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.10.2/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.10.2/manifests/metallb.yaml
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
# May require:
kubectl delete secret  -n metallb-system memberlist

#Following in metallb-config.yaml :

#___
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 192.168.1.8-192.168.1.15
#___

kubectl apply -f metallb-config.yaml

# Consider using metallb.universe.tf/allow-shared-ip to permit services to colocate on an IP
# Eg, in service:
#___
apiVersion: v1
kind: Service
metadata:
  name: cgen
  annotations:
    metallb.universe.tf/allow-shared-ip: ekvm
spec:
  selector:
    app: ekvm
  ports:
  - port: 5001
  externalTrafficPolicy: Local
  loadBalancerIP: 10.0.0.2
  type: LoadBalancer
'''
#___


# Install nginx ingress controller
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm show values ingress-nginx/ingress-nginx

# helm install [RELEASE_NAME] ingress-nginx/ingress-nginx [-f values.yaml]
helm install ingress-nginx-plain ingress-nginx/ingress-nginx --version 4.0.1
kubectl apply -f ingress-nginx-baremetal-github.yaml
# Or, all in one:
helm install ingress-nginx-plain ingress-nginx/ingress-nginx --version 4.0.1 -f ingress-nginx-baremetal-github.yaml


kubectl --namespace default get services -o wide -w ingress-nginx-controller

# Also install the LAN-facing nginx ingress controller:
#helm install -n ingress-nginx-lanf-ns ingress-nginx-lan-facing ingress-nginx/ingress-nginx --version 4.0.1 -f ingress-nginx-deploy-full-setup-lan-facing.yaml
#kubectl apply -f ingress-nginx-deploy-full-setup-lan-facing.yaml


# Set following values:
#  ingressClass: nginx-inet-facing
#...
#  service:
#    annotations: 
#      metallb.universe.tf/address-pool: inet-facing


# To uninstall:
# helm uninstall [RELEASE_NAME]
helm uninstall ingress-nginx-inet-facing

WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/mike/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/mike/.kube/config
NAME: ingress-nginx
LAST DEPLOYED: Wed Sep  1 10:24:54 2021
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The ingress-nginx controller has been installed.
It may take a few minutes for the LoadBalancer IP to be available.
You can watch the status by running 'kubectl --namespace default get services -o wide -w ingress-nginx-controller'

An example Ingress that makes use of the controller:

  apiVersion: networking.k8s.io/v1
  kind: Ingress
  metadata:
    annotations:
      kubernetes.io/ingress.class:
    name: example
    namespace: foo
  spec:
    rules:
      - host: www.example.com
        http:
          paths:
            - backend:
                serviceName: exampleService
                servicePort: 80
              path: /
    # This section is only required if TLS is to be enabled for the Ingress
    tls:
        - hosts:
            - www.example.com
          secretName: example-tls

If TLS is enabled for the Ingress, a Secret containing the certificate and key must also be provided:

  apiVersion: v1
  kind: Secret
  metadata:
    name: example-tls
    namespace: foo
  data:
    tls.crt: <base64 encoded cert>
    tls.key: <base64 encoded key>
  type: kubernetes.io/tls

# Miscellaneous nginx ingress related
# Get list of allocated ingress IPs:
kubectl get ingress -A

# To use, add the kubernetes.io/ingress.class: nginx annotation to your Ingress resources.

# To check if the ingress controller pods have started:
kubectl get pods -n ingress-nginx -l app.kubernetes.io/name=ingress-nginx --watch


# Install Rancher 2.6.0 into namespace cattle-system

kubectl create namespace cattle-system

#helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
#helm install rancher rancher-stable/rancher --version 2.5.9 --namespace cattle-system --set hostname=mitchell

# helm install rancher rancher-stable/rancher --version 2.5.9 --namespace cattle-system --set hostname=mitchell --set ingress.enabled="false"

helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
helm install rancher rancher-latest/rancher --version 2.6.0 --namespace cattle-system --set hostname=mitchell

# echo https://mitchell/dashboard/?setup=$(kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}')
# gives dashboard url at https://mitchell/dashboard/?setup=

kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}{{ "\n" }}'

# Browse to https://mitchell
# Set a specific password to use
# Set default view to "I want to create or manage mutliple clusters"
# Check Ts&Cs etc
# On next page set Rancher Server URL to https://mitchell

# Create a chart catalog
# Browse to 222.bitnami.com and login 
# From Apps & Marketplace choose Chart Repositories and select Create 
# Set Name to Bitnami
# Choose Target type radiobutton as http(s) URL to an index generated by Helm
# Set Index URL to https://charts.bitnami.com/bitnami
# Select Create to create the chart


# Set up Longhorn in Rancher:
# Pre-requisites:  bash, curl, findmnt, grep, awk and blkid
# Install open-iscsi:
sudo apt install -y open-iscsi

# In cluster Manager select "local" from Clusters tab at top left
# Select Projects/Namespaces and "Add Project", naming it "Storage" and leaving all fields at default
# Verify that the Project: Storage is visible in the Projects/Namespaces list, it will be empty
# Select local/local/Storage from the Clusters tab and select Apps then Launch
# Browse down to Longhorn and select it
# Leave following at default:
#	Name:									longhorn-system 
#	Required Namespace:						longhorn-system
#	Default Storage Class:					True
#	Expose app using Layer 7 Load Balancer:	False
# Change default replicas to 1


# Create an Ingress to allow external traffic to reach the Longhorn UI:
# Create a basic auth file auth
USER=Mike; PASSWORD=Cabbages1; echo "${USER}:$(openssl passwd -stdin -apr1 <<< ${PASSWORD})" >> auth

#Create a secret:
kubectl -n longhorn-system create secret generic basic-auth --from-file=auth

#Create an Ingress manifest longhorn-ingress.yml:
vi longhorn-ingress.yml

# Paste as follows:
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: longhorn-ingress
  namespace: longhorn-system
  annotations:
    # type of authentication
    nginx.ingress.kubernetes.io/auth-type: basic
    # prevent the controller from redirecting (308) to HTTPS
    nginx.ingress.kubernetes.io/ssl-redirect: 'false'
    # name of the secret that contains the user/password definitions
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    # message to display with an appropriate context why the authentication is required
    nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required '
    # custom max body size for file uploading like backing image uploading
    nginx.ingress.kubernetes.io/proxy-body-size: 10000m
spec:
  rules:
  - http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: longhorn-frontend
            port:
              number: 80

# Create the Ingress:
kubectl -n longhorn-system apply -f longhorn-ingress.yml

# View the ingress
kubectl -n longhorn-system get ingress
> NAME               CLASS    HOSTS   ADDRESS       PORTS   AGE
> longhorn-ingress   <none>   *       192.168.1.9   80      14s

# Or:
> NAME               CLASS    HOSTS   ADDRESS        PORTS   AGE
> longhorn-ingress   <none>   *       192.168.1.91   80      12s

# Browse to the ADDRESS to find the Longhorn GUI

# Or, From Workloads/longhorn-ui/80/HTTP:
https://192.168.1.8/#/dashboard

# Or select Longhorn from left hand menu then click "Manage storage system via UI"

# Set an NFS directory as backup
# Install nfs tools on rancher server:
apt install nfs-common

# Show mounts on nfs file server:
showmount -e owen
> Export list for owen:
> /mnt/Owen_shared/Public/s3_data/longhorn_nfs (everyone)

# To mount the share on a linux machine:
# mkdir /mnt/nfs
# mount -t nfs owen:/mnt/Owen_shared/Public/s3_data/longhorn_nfs /mnt/nfs

# In LH dashboard select Settings then scroll down to Backup Target and set it to "nfs://owen:/mnt/Owen_shared/Public/s3_data/longhorn_nfs"


# Set up an S3 share as backup:
# In LH dashboard select Settings then scroll down to Backup Target and set it to "s3:http://owen:9000/minio/longhorn-bucket/"
# In Cluster Manager select local/Storage then choose Resources/Secrets from top menu
# Create a secret named longhorn-owen-access-key having two Keys : AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY with the values populated form the AWS S3 server access key and secret access key



# Deleting obnoxious namespaces see here: https://computingforgeeks.com/how-to-force-delete-a-kubernetes-namespace/
# If the following doesn't work:
kubectl delete ns NAMESPACE_TO_DELETE 

kubectl get ns NAMESPACE_TO_DELETE -o json > tmp.json
# Edit tmp.json so as to remove the contents of the finalizers value.  ie:
#     "spec": {
#        "finalizers": [
#            "kubernetes"
#        ]
#      }; 
# becomes:
#    "spec": {
#        "finalizers": []
#    },
#
# In another terminal run:
kubectl proxy
#
# In first terminal run:
curl -k -H "Content-Type: application/json" -X PUT --data-binary @tmp.json http://127.0.0.1:8001/api/v1/namespaces/NAMESPACE_TO_DELETE/finalize
#
# Close the terminal running kubectl proxy so the IP address it uses is released for next time


# Install an apache app as a demo:
helm install my-release -f apache1.local-inet-facing.yaml bitnami/apache
helm install my-release -f apache1.local-baremetal-default.yaml bitnami/apache
helm install my-release -f apache1.local-baremetal-default-local.yaml bitnami/apache
helm install my-release -f apache1.gitlab-web-baremetal-default-local.yaml bitnami/apache
helm install my-release -f apache1.local-baremetal-beginner-onweb.yaml bitnami/apache
helm install my-release -f apache1.local-beginner-onweb.yaml bitnami/apache

# To uninstall it:
helm uninstall my-release

# To upgrade it:
helm upgrade my-release -f apache1.local-beginner-onweb.yaml bitnami/apache

# Alternatively, from files:
kubectl create configmap required-files --from-file=index.html --from-file=style.css --from-file=85186_orig.jpg 

kubectl describe configmaps required-files